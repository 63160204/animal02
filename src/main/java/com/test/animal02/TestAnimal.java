/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.animal02;

/**
 *
 * @author patthamawan
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("NingNong");
        h1.eat();
        h1.walk();
        h1.run();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));

        Animal a1 = h1;
        System.out.println("a1 is land animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animal ? " + (a1 instanceof Reptile));
        System.out.println("---------------------------------------------");

        Dog d1 = new Dog("BoBo");
        d1.eat();
        d1.walk();
        d1.run();
        System.out.println("d1 is animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is land animal ? " + (d1 instanceof LandAnimal));

        Animal a2 = d1;
        System.out.println("a2 is land animal ? " + (a2 instanceof LandAnimal));
        System.out.println("a2 is reptile animal ? " + (a2 instanceof Reptile));
        System.out.println("---------------------------------------------");

        Cat c1 = new Cat("SomSi");
        c1.eat();
        c1.walk();
        c1.run();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is land animal ? " + (c1 instanceof LandAnimal));

        Animal a3 = c1;
        System.out.println("a3 is land animal ? " + (a3 instanceof LandAnimal));
        System.out.println("a3 is reptile animal ? " + (a3 instanceof Reptile));
        System.out.println("---------------------------------------------");

        Bird b1 = new Bird("JibJib");
        b1.eat();
        b1.walk();
        b1.fly();
        System.out.println("b1 is animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is land animal ? " + (b1 instanceof Poultry));

        Animal a4 = b1;
        System.out.println("a4 is land animal ? " + (a4 instanceof Reptile));
        System.out.println("a4 is reptile animal ? " + (a4 instanceof LandAnimal));
        System.out.println("---------------------------------------------");

        Bat b2 = new Bat("MaeKai");
        b2.eat();
        b2.walk();
        b2.fly();
        System.out.println("b2 is animal ? " + (b2 instanceof Animal));
        System.out.println("b2 is land animal ? " + (b2 instanceof Poultry));

        Animal a5 = b2;
        System.out.println("a5 is land animal ? " + (a5 instanceof Reptile));
        System.out.println("a5 is reptile animal ? " + (a5 instanceof LandAnimal));
        System.out.println("---------------------------------------------");

        Snake s1 = new Snake("JingJai");
        s1.eat();
        s1.walk();
        s1.crawl();
        System.out.println("s1 is animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is land animal ? " + (s1 instanceof Reptile));

        Animal a6 = s1;
        System.out.println("a6 is land animal ? " + (a6 instanceof Poultry));
        System.out.println("a6 is reptile animal ? " + (a6 instanceof LandAnimal));
        System.out.println("---------------------------------------------");

        Crocodile c2 = new Crocodile("KraiThong");
        c2.eat();
        c2.walk();
        c2.crawl();
        System.out.println("c2 is animal ? " + (c2 instanceof Animal));
        System.out.println("c2 is land animal ? " + (c2 instanceof Reptile));

        Animal a7 = c2;
        System.out.println("a7 is land animal ? " + (a7 instanceof Reptile));
        System.out.println("a7 is reptile animal ? " + (a7 instanceof LandAnimal));
        System.out.println("---------------------------------------------");

        Fish f1 = new Fish("Nemo");
        f1.eat();
        f1.walk();
        f1.swim();
        System.out.println("f1 is animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is land animal ? " + (f1 instanceof AquaticAnimal));

        Animal a8 = f1;
        System.out.println("a8 is land animal ? " + (a8 instanceof Reptile));
        System.out.println("a8 is reptile animal ? " + (a8 instanceof LandAnimal));
        System.out.println("---------------------------------------------");

        Crab c3 = new Crab("Mr.Crab");
        c3.eat();
        c3.walk();
        c3.swim();
        System.out.println("c3 is animal ? " + (c3 instanceof Animal));
        System.out.println("c3 is land animal ? " + (c3 instanceof AquaticAnimal));

        Animal a9 = c3;
        System.out.println("a9 is land animal ? " + (a9 instanceof Reptile));
        System.out.println("a9 is reptile animal ? " + (a9 instanceof AquaticAnimal));
        System.out.println("---------------------------------------------");
    }
}
